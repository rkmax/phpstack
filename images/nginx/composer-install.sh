#!/bin/bash

cd /var/www

# Install PHP dependencies
/composer.phar install

# Install grunt task runner
hash grunt || {
  npm install -g grunt-cli
}

# Install NPM local dependencies
npm install

# Run default tasks
grunt

# Build the elasticsearch index
app/console -e=prod fos:elastica:populate
